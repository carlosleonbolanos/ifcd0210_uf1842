const TECLAS = [
     // Códigos de teclas obtenidos a través de https://keycode.info
     // Teclas Edición
     35 // FIN
     , 36 // INICIO
     // Cursores
     , 37 // Flecha Izquierda
     , 38 // Flecha Arriba
     , 39 // Flecha Derecha
     , 40 // Flecha Abajo
     // Teclado QWERTY
     , 48 // 0
     , 49 // 1
     , 50 // 2
     , 51 // 3
     , 52 // 4
     , 53 // 5
     , 54 // 6
     , 55 // 7
     , 56 // 8
     , 57 // 9
     , 88 // X
     , 89 // Y
     , 90 // Z
     , 46 // DELETE
     , 8 // BACKSPACE
     // Teclado Numérico
     , 96 // 0
     , 97 // 1
     , 98 // 2
     , 99 // 3
     , 100 // 4
     , 101 // 5
     , 102 // 6
     , 103 // 7
     , 104 // 8
     , 105 // 9
];
const PROVINCIAS = ["Álava", "Albacete", "Alicante", "Almería", "Ávila", "Badajoz", "Baleares", "Barcelona", "Burgos", "Cáceres", "Cádiz", "Castellón", "Ciudad Real", "Córdoba", "La Coruña", "Cuenca", "Gerona", "Granada", "Guadalajara", "Guipúzcoa", "Huelva", "Huesca", "Jaén", "León", "Lérida", "La Rioja", "Lugo", "Madrid", "Málaga", "Murcia", "Navarra", "Orense", "Asturias", "Palencia", "Las Palmas", "Pontevedra", "Salamanca", "Santa Cruz de Tenerife", "Cantabria", "Segovia", "Sevilla", "Soria", "Tarragona", "Teruel", "Toledo", "Valencia", "Valladolid", "Vizcaya", "Zamora", "Zaragoza", "Ceuta", "Melilla"];
const NIES = ["X", "Y", "Z"];
const LETRAS = ["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];

function load() {
     with (window) {
          addEventListener("keydown", event => {
               if (validador(event)) { return event.preventDefault(); }
          }, false)
          addEventListener("keyup", function () {
               calculo(document.getElementById("dni").value);
          }, false);
     }

     document.getElementById("cp").addEventListener("keyup", buscarprovincia, false);
     document.getElementById("altura").addEventListener("keyup", calculoIMC, false);
     document.getElementById("altura").addEventListener("keypressed", calculoIMC, false);
     document.getElementById("peso").addEventListener("keyup", calculoIMC, false);
     document.getElementById("peso").addEventListener("keypressed", calculoIMC, false);
}

function validador(evento) {
     const longdni = document.getElementById("dni").value.length;

     if (TECLAS.indexOf(evento.keyCode) == -1) {
          if (evento.target.id == "dni") return true;
     } else if (longdni >= 1 && longdni < 8 && evento.keyCode > 87 && evento.keyCode < 91) {
          return true;
     }
}

function calculo(dni) {
     var resto = 0;
     dni = dni.toUpperCase();
     var resultado = document.getElementById("letra");

     if (isNaN(dni[0])) {
          dni = dni.replace(dni[0], NIES.indexOf(dni[0]));
     }

     if (dni.length < 8) {
          resultado.value = "";
          return;
     }

     var resto = dni % 23;
     resultado.value = LETRAS[resto];
}

function buscarprovincia() {
     const cp = document.getElementById("cp").value;
     const provincia = document.getElementById("provincia");
     if (cp.length == 5) {
          var i = Number(cp.substring(0, 2)) - 1;
          provincia.value = PROVINCIAS[i];
     } else {
          provincia.value = "";
     }
}

function calculoIMC() {
     const edad = document.getElementById("fechanacimiento").value;
     const altura = document.getElementById("altura").value;
     const peso = document.getElementById("peso").value;
     const resultado = document.getElementById("resultado");
     const mantpeso = document.getElementById("pesomantval");
     const bajapeso = document.getElementById("pesobajaval");
     const sexo = document.getElementById("sexo").value;
     const ejercicio = Number(document.getElementById("actividad").value);

     if (edad == 0 || isNaN(edad)) {
          resultado.innerHTML = "Por favor, cumplimente la ficha personal para proceder.";
     }

     if (isNaN(altura) || altura < 0 || isNaN(peso) || peso < 0) {
          resultado.innerHTML = "Por favor, introduzca o corríja los datos en los campos de Altura y Peso para realizar el cálculo.";
     }

     var imc = (Number(peso.replace(",", ".")) / Math.pow(Number(altura.replace(",", ".")), 2)).toFixed(1);

     var calmant = mantenimientopeso()
     console.log(calculoEdad());
     resultado.innerHTML = `Su IMC es de ${imc}.`;

     mantpeso.innerHTML = ` ${mantenimientopeso(sexo, peso, Number(altura.replace(",", ".")) * 100, calculoEdad(), ejercicio).toFixed(2)} cal.`;
     bajapeso.innerHTML = ` ${(mantenimientopeso(sexo, peso, Number(altura.replace(",", ".")) * 100, calculoEdad(), ejercicio) - 1000).toFixed(2)} cal.`;
}

function calculoEdad() {
     const fechatxt = document.getElementById("edad");
     const fecha = document.getElementById("fechanacimiento").value;
     var fechanac = new Date(fecha)
     var hoy = new Date();

     var edad = hoy.getFullYear() - fechanac.getFullYear();

     var m = hoy.getMonth() - fechanac.getMonth();

     if (m < 0 || (m === 0 && hoy.getDate() < fechanac.getDate())) {
          edad = edad--;
     }
     fechatxt.value = edad;
     return Number(edad);
}

function TMB(sexo, peso, altura, edad) {
     var tmb;
     if (sexo == 0) {
          tmb = 66 + (13.7 * peso) + (5 * altura) - (6.75 * edad);
     } else if (sexo == 1) {
          tmb = 655 + (9.6 * peso) + (1.8 * altura) - (4.7 * edad);
     }
     return tmb;
}

function mantenimientopeso(sexo, peso, altura, edad, constante) {
     console.log(sexo, peso, altura, edad, constante);
     return (TMB(sexo, peso, altura, edad) * constante);
}